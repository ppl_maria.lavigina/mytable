const menu = [
  { charityName: "Earth Trust", distance: 30, total: 7, year: 2008 },
  { charityName: "Animal Trust", distance: 10, total: 23, year: 2001 },
  { charityName: "Childrean Trust", distance: 12, total: 75, year: 2020 },
  { charityName: "Culture Trust", distance: 5, total: 354, year: 2005 },
  { charityName: "Inovation Trust", distance: 45, total: 109, year: 2019 },
];

const div4Table = document.getElementById("myTable"), //div table
  byMenu = document.querySelector("div.button-group"); //div button

function makeTable() {
  div4Table.innerHTML = ""; //to clear the table

  let t = div4Table.appendChild(document.createElement("table")),
    tH = t.createTHead(), //creates empty head
    tB = t.createTBody(); //creates empty body

  let tC = t.setAttribute("id", "idTable"); //table has an id = "idTable"

  charityNameRow = tH.insertRow(); //create row name charityNameRow, the first row with the headings

  charityNameRow.insertCell().textContent = "Charity Name"; //charityNameRow.td.setAttribute("data-type", "string");

  let td = document
    .getElementById("idTable") // table
    .getElementsByTagName("tr")[0] // green row (heading)
    .getElementsByTagName("td")[0]; // cell of head (charity name)
  td.setAttribute("data-type", "string"); // what kind of data in the column

  charityNameRow.insertCell().textContent = "Distance";
  td = document
    .getElementById("idTable")
    .getElementsByTagName("tr")[0] // green row (heading)
    .getElementsByTagName("td")[1]; // cell of head (Distance)
  td.setAttribute("data-type", "number"); // what kind of data in the column

  charityNameRow.insertCell().textContent = "Total";
  td = document
    .getElementById("idTable")
    .getElementsByTagName("tr")[0] // green row (heading)
    .getElementsByTagName("td")[2]; // cell of head (Total)
  td.setAttribute("data-type", "number"); // what kind of data in the column

  charityNameRow.insertCell().textContent = "Year";
  td = document
    .getElementById("idTable") //table
    .getElementsByTagName("tr")[0] // green row (heading)
    .getElementsByTagName("td")[3]; // cell of head (Year)
  td.setAttribute("data-type", "number"); // what kind of data in the column

  const array1 = [menu.forEach.charityName];
  array1.sort();

  menu.forEach((charity) => {
    let nRow = tB.insertRow();
    nRow.insertCell().textContent = charity.charityName;
    nRow.insertCell().textContent = charity.distance;
    nRow.insertCell().textContent = charity.total;
    nRow.insertCell().textContent = charity.year;
  });
}

function sortTable() {
  //
  let table = document.getElementById("idTable"); // variable: table
  let sortedRows = Array.from(table.rows) // working with rows
    .slice(1) // takes all the data without the head
    .sort((rowA, rowB) =>
      rowA.cells[0].innerHTML > rowB.cells[0].innerHTML ? 1 : -1
    ); // changes location by a..b..c..

  table.tBodies[0].append(...sortedRows);
}
